"use strict";
// Setting up the database connection
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bookshelf_1 = __importDefault(require("bookshelf"));
const knex_1 = __importDefault(require("knex"));
const bookshelfInstace = bookshelf_1.default(
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
knex_1.default({
    client: "mysql",
    connection: {
        host: "127.0.0.1",
        user: "root",
        password: "naucme",
        database: "naucme_test",
        charset: "utf8",
    },
}));
// Defining models
const Teacher = bookshelfInstace.model("Teacher", {
    tableName: "teachers",
});
const Teach = bookshelfInstace.model("Teach", {
    tableName: "teaches",
});
const Interested = bookshelfInstace.model("Interested", {
    tableName: "interested",
});
//# sourceMappingURL=service.js.map