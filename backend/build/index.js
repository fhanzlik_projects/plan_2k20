"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = express_1.default();
var HandlerResponseStatus;
(function (HandlerResponseStatus) {
    HandlerResponseStatus[HandlerResponseStatus["OK"] = 0] = "OK";
})(HandlerResponseStatus = exports.HandlerResponseStatus || (exports.HandlerResponseStatus = {}));
/* const registerService = <TData>(
    path: string,
    handler: Handler<unknown, TData>,
) =>
    app.post(path, (req, res) => {
        console.log("")
        res.send(JSON.stringify(handler(JSON.parse(req.body))))
    })

registerService("/register/teacher", registerTeacher)
 */
app.get("/", (_, res) => res.send("blah"));
const port = 3000;
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});
//# sourceMappingURL=index.js.map