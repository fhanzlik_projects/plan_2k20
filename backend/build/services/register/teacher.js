"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("../..");
exports.registerTeacher = teacher => {
    return { status: __1.HandlerResponseStatus.OK, data: JSON.stringify(teacher) };
};
//# sourceMappingURL=teacher.js.map