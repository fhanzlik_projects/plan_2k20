CREATE DATABASE if not exists  naucme_test;
use naucme_test;

create table teaches (
    id int PRIMARY KEY,
    teach_name varchar(255) NOT NULL,
    teach_type varchar(255) NOT NULL,
    teach_detail text NOT null,
    teach_price varchar(255) not null
);

create table teacher_credentials(
    id int PRIMARY KEY AUTO_INCREMENT,
    teacher_name varchar(255) not null,
    teacher_phone varchar(255) not null,
    teacher_email varchar(255) not null,
    teacher_pass varchar(255) not null,
    teach_id int default 0
);

create table interested_studs(
    id int PRIMARY KEY AUTO_INCREMENT,
    teach_id int,
    stud_email varchar(255),
    stud_name varchar(255)
);