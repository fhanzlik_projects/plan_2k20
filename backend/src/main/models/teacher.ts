import { TeachId } from "./teach"

export interface TeacherModel {
	id: string
	name: string
	teachId: TeachId
}
