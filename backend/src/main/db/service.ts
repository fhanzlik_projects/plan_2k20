// Setting up the database connection

import bookshelf from "bookshelf"
import knex from "knex"

const bookshelfInstace = bookshelf(
	// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
	// @ts-ignore
	knex({
		client: "mysql",
		connection: {
			host: "127.0.0.1",
			user: "root",
			password: "naucme",
			database: "naucme_test",
			charset: "utf8",
		},
	}),
)

// Defining models
const Teacher = bookshelfInstace.model("Teacher", {
	tableName: "teachers",
})

const Teach = bookshelfInstace.model("Teach", {
	tableName: "teaches",
})

const Interested = bookshelfInstace.model("Interested", {
	tableName: "interested",
})
