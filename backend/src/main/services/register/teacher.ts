import { Handler, HandlerResponseStatus } from "../.."

export const registerTeacher: Handler<unknown, string> = teacher => {
	return { status: HandlerResponseStatus.OK, data: JSON.stringify(teacher) }
}
