import { registerTeacher } from "./services/register/teacher"
import express from "express"

const app = express()

export enum HandlerResponseStatus {
	OK,
}

interface HandlerResponse<TData> {
	status: HandlerResponseStatus
	data: TData
}

export interface Handler<TInput, TData> {
	(input: TInput): HandlerResponse<TData>
}

/* const registerService = <TData>(
	path: string,
	handler: Handler<unknown, TData>,
) =>
	app.post(path, (req, res) => {
		console.log("")
		res.send(JSON.stringify(handler(JSON.parse(req.body))))
	})

registerService("/register/teacher", registerTeacher)
 */
app.get("/", (_, res) => res.send("blah"))

const port = 3000

app.listen(port, () => {
	console.log(`Listening on port ${port}`)
})
