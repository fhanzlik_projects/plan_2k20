import * as React from "react"
import { LoginOkno } from "../src/main/LoginOkno"
import Head from "next/head"
import styled from "styled-components"

export default () => (
	<>
		<Head>
			<title>Naučmě.cz přihlášení</title>
		</Head>
		<Wrapper>
			<LoginOkno>
				<form>
					<h2 style={{ fontFamily: "Open Sans" }}>E-mail:</h2>
					<input name="teacherEmail" />
					<h2 style={{ fontFamily: "Open Sans" }}>Heslo:</h2>
					<input name="teacherPass" type="password" />
				</form>
			</LoginOkno>
		</Wrapper>
	</>
)

const Wrapper = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	height: 100vh;
`
