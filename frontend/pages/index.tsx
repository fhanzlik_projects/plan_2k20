
import * as React from "react";
import Head from "next/head";
import {Nav} from "../src/main/Nav";
import {ChciButton} from "../src/main/ChciButton";
import {Clanek} from "../src/main/Clanek";
export default () => (
    <>


        <Head>

            <title>Naučmě.cz</title>
        </Head>
        <></>
        <div style={{backgroundColor: "" , width: "100vw" , height: "100vh"}}>
            <Nav></Nav>
        <h1 style={{textAlign: "center", fontFamily: "Open Sans", fontSize: "50px"}}>Vítejte na Naučmě.cz!</h1>
            <Clanek><h1 style={{textAlign: "center", fontFamily: "Open Sans", fontSize: "30px"}}>Chcete se něco naučit?</h1><p style={{fontFamily: "Open Sans", fontSize: "18px"}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur eaque exercitationem in magnam maiores modi nam sapiente sint totam unde. Aperiam doloremque ducimus eos esse ipsa quas, sed veniam veritatis?</p><ChciButton url={"/teaches/list"}>Chci se učit!</ChciButton></Clanek>

            <Clanek><h1 style={{textAlign: "center", fontFamily: "Open Sans", fontSize: "30px"}}>Umíte něco, co byste chtěli učit někoho jiného?</h1><p style={{fontFamily: "Open Sans", fontSize: "18px"}}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita id impedit inventore labore placeat sit! Dolor, magni, quia. Error, excepturi libero molestias nam nihil odit optio quis tempore veritatis voluptatibus?</p><ChciButton url={"/sign-teacher"}>Chci učit!</ChciButton></Clanek>
        </div>


    </>

)
