import App from "next/app"
import React from "react"
import Head from "next/head";

export default class extends App {
	render() {
		const { Component, pageProps } = this.props
		return <><Head><link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet"/><link rel="icon" href="/static/favicon.ico"/></Head><Component {...pageProps} /></>
	}
}
