import styled from "styled-components";


const ChciButtonInner = styled.a`
 width: 200px;
 height: 100px;
 position: absolute;
 border-style: none;
 transition: background-color 0.5s;
 font-size: 30px;
 font-family: Open Sans;
 border-radius: 10px;
display: flex;
justify-content: center;
background-color: rgb(191, 191, 191);
line-height: 100px;
text-decoration: none;
color: black;
 
 :hover {
  background-color: grey;
  color: white;
 }
`
const ChciButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
`
export const ChciButton: React.FC<{url:string}> = ({children,url})=>(<ChciButtonWrapper><ChciButtonInner href={url}>{children}</ChciButtonInner></ChciButtonWrapper>)
