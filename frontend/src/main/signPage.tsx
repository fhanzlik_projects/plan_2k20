import React from "react";
import Head from "next/head";
import {Nav} from "./Nav";
import styled from "styled-components";

const TeacherStyleForm = styled.form`
    display: flex;
    flex-direction: column;
    width: 20%;
    align-content: center;
    font-family: 'Montserrat', sans-serif;
    justify-content: center;
`;


export function SignPage(){
    return(
        <>
            <Head>
                <title>Registrace Učitele | Nauč.mě</title>
            </Head>
            <Nav />
            <div>
                <TeacherStyleForm>
                    <label htmlFor="teacherName">Tvoje jméno</label>
                    <input name="teacherName" placeholder="Jméno" id="teacherName"/>

                    <label htmlFor="teacherEmail">Tvůj Email</label>
                    <input name="teacherEmail" placeholder="email" id="teacherName"/>

                    <label htmlFor="teacherPhone">Tvůj telefon</label>
                    <input name="teacherPhone" placeholder="telefonní číslo" id="teacherPhone" />

                    <label htmlFor="teachType">Kategorie Výuky</label>
                    <input name="teachType" placeholder="Kategorie toho co chceš naučit :)" id="teachType" />

                    <label htmlFor="teachPrice">Cena</label>
                    <input name="teachPrice" placeholder="napiš 0, nebo nech prázdné, pokud se chceš o znalosti dělit zdarma :)" id="teachPrice" />

                    <label htmlFor="teachDesc">Popis Výuky</label>
                    <input name="teachDesc" placeholder="Popiš co chceš naučít :)" type="textarea" id="teachDesc"/>
                    <input type="submit" onClick={async ()=>{
                        // @ts-ignore
                        let a = await fetch("localhost:3030/api/teacher/register", {
                            method: 'POST',
                            body:JSON.stringify({
                                teacherName: (document.getElementById('teacherName') as HTMLInputElement).value,
                                teacherEmail: (document.getElementById('teacherEmail') as HTMLInputElement).value,
                                teacherPhone: (document.getElementById('teacherPhone')as HTMLInputElement).value,
                                teachType: (document.getElementById('teachType') as HTMLInputElement).value,
                                teachPrice: (document.getElementById('teachPrice') as HTMLInputElement).value,
                                teachDesc: (document.getElementById('teachDesc') as HTMLInputElement).value,
                            }),
                        })
                        let b = await a.json();
                        console.log(b)
                    }} />
                </TeacherStyleForm>

            </div>
        </>
    )
}