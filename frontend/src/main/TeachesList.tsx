import React from "react"
import Head from "next/head"
import {Nav} from "./Nav"
import styled from "styled-components";

const StyledTeachList = styled.div`
    display: flex;
    font-family: 'Montserrat', sans-serif;
`

const StyledTeach = styled.div`
    border: 0.5px solid lightgrey;
    border-radius: 5px;
    margin: 5px;
    padding: 10px;
    transition: all 0.2s;
    
    :hover{
      background: #135154;
      color: white;
    }
`

export function TeachesList(){
    return(
        <>
            <Head>
                <title>Co se můžu naučit | Nauč.mě</title>
            </Head>
            <Nav />
            <StyledTeachList className="teachListContainer">
                <StyledTeach className="teach">
                    <h3 className="teach-title"> Jméno přednášky</h3>
                    <div className="teach-detail">
                        <span className="teach-category">Kategorie</span> |
                        <span className="teach-price"> Cena</span>
                        <p className="teach-description">Popis</p>
                    </div>
                </StyledTeach>
            </StyledTeachList>
        </>
    )
}