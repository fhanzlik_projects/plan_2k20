import styled from "styled-components"

export const LoginOkno = styled.a`
	position: center;
	height: 300px;
	width: 300px;
	background-color: white;
	margin: auto;
	padding: 2rem;
	border-radius: 20px;
	box-shadow: 1px 5px 5px 5px grey;
`
