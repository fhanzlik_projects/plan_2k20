import styled from "styled-components";
import * as React from "react";

const NavButton = styled.a`
 width: 150px;
 height: auto;
 float: right;
 border-style: none;
 transition: background-color 0.5s;
 background-color: white;
 text-align: center;
 line-height: 40px;
 margin-top: 10px;
 margin-bottom: 10px;
 font-family: Open Sans;
 text-decoration: none;
 color: black;
 cursor: default;
 
 :hover {
  background-color: grey;
  color: white;
 }
`
export const Nav: React.FC=()=>(
    <div style={{width: "100%" , height: "60px" , backgroundColor: "#009fb7"}}>

        <NavButton href={"login"}>Přihlásit se</NavButton>

    </div>
)