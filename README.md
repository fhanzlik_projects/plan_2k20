## Setup
### Frontend
* Run `npm i`
* now you can `npm test` or `npx next`

### Backend
* `npm i`
* `node .`
**NOTE: if you run without doker, edit index.ts to listen on port 3030 instead of 3000**

### Docker
If you want to use Docker, you can just pull dockerfiles and `docker-compose.yml` from `development-docker` branch
* Go `docker-compose up` and wait for it to finish
* Now you should be up and running
* Ports
    * 3000 - Frontend
    * 3030 - Backend
    * 3033 - MariaDB

## IDEs
### Frontend
Fully set up for WebStorm and VSCode (workspace)

### Backend
Set up for VSCode (workspace)